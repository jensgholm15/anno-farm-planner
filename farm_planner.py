
"""
This script is a little utility to compute layouts of plantations in anno.
"""

import sys
import math

def get_int(message):

    user_input = raw_input(message)

    try:
       result = int(user_input)
    except:
        print "input: '{}' is is not valid, only integer input is valid.".format(user_input)
        exit(1)

    return result

def main():

    # Get the user input
    building_width = get_int("Building width: ")
    building_height = get_int("Building height: ")
    modules_per_building = get_int("module per building: ")
    number_of_buildings = get_int("number of buildings: ")
    blocking_tiles = get_int("blocking tiles: ")

    # Summerize problem for user
    print "Building dimensions: {}x{}, with {} modules ".format(building_width, building_height, modules_per_building)
    print "solutions for {} buildings".format(number_of_buildings)

    # Compute solutions

    # We want to find all integer pairs that satisfies `w * h = number_of_buildings * modules_per_building`

    area = blocking_tiles + number_of_buildings * (modules_per_building + (building_width * building_height))

    solutions = []

    lower_range = building_width * number_of_buildings
    upper_range = int(math.ceil(math.sqrt(area)))

    for i in range(lower_range, upper_range):

        if area % i == 0 and (building_height * number_of_buildings) < area / i:

            solutions.append((i, area / i))

    for (width, height) in solutions:

        print "{}x{}".format(width, height)


if __name__ == "__main__":

    main()

